const colors = require("tailwindcss/colors");

module.exports = {
	purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
	darkMode: false,
	theme: {
		extend: {
			colors: {
				gray: colors.trueGray,
				sky: colors.sky,
				orange: colors.orange,
				indigo: colors.indigo,
				green: colors.green,
				rose: colors.rose,
				midnight: "#100b26"
			}
		}
	},
	variants: {
		extend: {
			backgroundColor: ["disabled"]
		}
	},
	plugins: []
};
