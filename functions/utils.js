async function *lineIterator(reader) {
	const utf8Decoder = new TextDecoder("utf-8");

	let newlineRegex = /\r\n|\n|\r/gm;
	let index = 0;

	let { value, done } = await reader.read();
	value = value ? utf8Decoder.decode(value, { stream: true }) : "";

	for (;;) {
		let result = newlineRegex.exec(value);

		if (!result) {
			if (done) break;

			let remainder = value.substr(index);
			({ value, done } = await reader.read());
			value = remainder + (value ? utf8Decoder.decode(value, { stream: true }) : "");
			index = newlineRegex.lastIndex = 0;

			continue;
		}

		yield value.substring(index, result.index);
		index = newlineRegex.lastIndex;
	}
	if (index < value.length)
		yield value.substr(index);
}

export const parseCsv = async (stream, headers, skip) => {
	let result = [];
	let index = 0;

	for await (let line of lineIterator(stream.getReader())) {
		if (index < skip) {
			index += 1;
			continue;
		}

		result[index-skip] = {};

		let lineParts = line.split(",");

		for (let [i, part] of lineParts.entries()) {
			if (part[0] === "\"" && part[part.length - 1] === "\"")
				part = part.substr(1, part.length - 2);

			if (i < headers.length)
				result[index-skip][headers[i]] = part;
			else
				break;
		}

		index += 1;
	}

	return result;
};
