/* eslint-disable camelcase */
import * as jose from "jose";
import cookie from "cookie";

const generateID = (length) => {
	let result = "";
	let characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (let i = 0; i < length; i++)
		result += characters.charAt(Math.floor(Math.random() * characters.length));

	return result;
};

export const onRequestGet = async ({ request, env }) => {
	try {
		let urlParts = request.url.split("?");
		let params = new URLSearchParams(urlParts.length === 2 ? urlParts[1] : "");

		if (params.get("error") || !params.get("code")) {
			let error = params.get("error");

			if (!error)
				error = "The request could not be completed";

			return new Response(null, {
				headers: {
					location: `${env.BASE_URL}?error=${encodeURI(error)}`
				},
				status: 302
			});
		}

		let code = params.get("code");
		let response = await fetch("https://discord.com/api/v9/oauth2/token", {
			method: "POST",
			headers: {
				"content-type": "application/x-www-form-urlencoded"
			},
			body: new URLSearchParams({
				redirect_uri: env.BASE_URL + "/api/discord/callback",
				grant_type: "authorization_code",
				code: code,
				client_id: env.DISCORD_CLIENT_ID,
				client_secret: env.DISCORD_CLIENT_SECRET
			})
		});

		if (!response.ok) {
			return new Response(null, {
				headers: {
					location: `${env.BASE_URL}?error=${encodeURI("The code provided was invalid or an error occurred")}`
				},
				status: 302
			});
		}

		let { access_token, expires_in } = await response.json();

		response = await fetch("https://discord.com/api/v9/users/@me", {
			method: "GET",
			headers: {
				authorization: "Bearer " + access_token
			}
		});

		if (!response.ok) {
			return new Response(null, {
				headers: {
					location: `${env.BASE_URL}?error=${encodeURI("An unknown error occurred whilst fetching your profile")}`
				},
				status: 302
			});
		}

		let user = await response.json();
		let id = generateID(16);

		await env.SESSIONS.put(id, JSON.stringify(user), {
			expirationTtl: expires_in - 10
		});

		let privateKey = await jose.importPKCS8(env.PRIVATE_KEY, "RS256");

		let token = await new jose.SignJWT({ id })
			.setProtectedHeader({ alg: "RS256", typ: "JWT" })
			.setExpirationTime(Math.floor(Date.now() / 1000) + expires_in - 10)
			.setIssuer("CompBot Gateway")
			.sign(privateKey);

		let cookieString = cookie.serialize("token", token, {
			httpOnly: true,
			secure: true,
			maxAge: expires_in - 10,
			path: "/"
		});

		return new Response(null, {
			headers: {
				location: env.BASE_URL,
				"set-cookie": cookieString
			},
			status: 302
		});
	} catch (err) {
		let payload = {
			error: true,
			message: "An unknown error occurred"
		};

		if (env.ENVIRONMENT === "development")
			payload.error = err.stack;

		return new Response(JSON.stringify(payload), {
			status: 500,
			headers: {
				"content-type": "application/json"
			}
		});
	}
};
