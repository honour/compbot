export const onRequestGet = async ({ env }) => {
	return new Response(null, {
		headers: {
			location: "https://discord.com/api/oauth2/authorize?" +
				"response_type=code&" +
				`client_id=${env.DISCORD_CLIENT_ID}&` +
				"scope=identify&" +
				`redirect_uri=${encodeURI(env.BASE_URL + "/api/discord/callback")}`
		},
		status: 302
	});
};
