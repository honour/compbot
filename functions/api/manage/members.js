import * as jose from "jose";
import cookie from "cookie";
import { parseCsv } from "../../utils";

export const onRequestPost = async ({ request, env }) => {
	try {
		let cookieHeader = request.headers.get("cookie");

		if (!cookieHeader) {
			return new Response(JSON.stringify({
				error: true,
				message: "You must login with Discord to use this endpoint"
			}), {
				status: 401,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		let cookies = cookie.parse(cookieHeader);
		let token = cookies.token;

		if (!token) {
			return new Response(JSON.stringify({
				error: true,
				message: "You must login with Discord to use this endpoint"
			}), {
				status: 401,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		let publicKey = await jose.importSPKI(env.PUBLIC_KEY, "RS256");
		let session;

		try {
			let { payload } = await jose.jwtVerify(token, publicKey);

			session = await env.SESSIONS.get(payload.id, { type: "json" });

			if (!session) {
				return new Response(JSON.stringify({
					error: true,
					message: "Your session has expired, please login again"
				}), {
					status: 401,
					headers: {
						"content-type": "application/json"
					}
				});
			}
		} catch (_) {
			return new Response(JSON.stringify({
				error: true,
				message: "Authorization token was malformed"
			}), {
				status: 401,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		let ids = env.ADMIN_IDS.split(",");

		if (!ids.includes(session.id)) {
			return new Response(JSON.stringify({
				error: true,
				message: "You do not have permission to use this endpoint"
			}), {
				status: 401,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		let memberships = await parseCsv(request.body, ["name", "email", "type"], 1);
		let members = {};

		for (let membership of memberships) {
			if (!members[membership.email] && env.MEMBERSHIP_TYPES.split(",").includes(membership.type)) {
				members[membership.email] = {
					name: membership.name,
					type: membership.type
				};
			}
		}

		await env.MEMBERS.put("members", JSON.stringify(members));

		return new Response(JSON.stringify({
			error: false,
			message: "Member list updated successfully"
		}), {
			headers: {
				"content-type": "application/json"
			}
		});
	} catch (err) {
		let payload = {
			error: true,
			message: "An unknown error occurred"
		};

		if (env.ENVIRONMENT === "development")
			payload.error = err.stack;

		return new Response(JSON.stringify(payload), {
			status: 500,
			headers: {
				"content-type": "application/json"
			}
		});
	}
};
