import * as jose from "jose";
import cookie from "cookie";

export const onRequestGet = async ({ request, env }) => {
	try {
		let cookieHeader = request.headers.get("cookie");

		if (!cookieHeader) {
			return new Response(JSON.stringify({
				error: true,
				message: "You must login with Discord to use this endpoint"
			}), {
				status: 401,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		let cookies = cookie.parse(cookieHeader);
		let token = cookies.token;

		if (!token) {
			return new Response(JSON.stringify({
				error: true,
				message: "You must login with Discord to use this endpoint"
			}), {
				status: 401,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		let publicKey = await jose.importSPKI(env.PUBLIC_KEY, "RS256");

		try {
			let { payload } = await jose.jwtVerify(token, publicKey);

			let session = await env.SESSIONS.get(payload.id, { type: "json" });

			if (!session) {
				return new Response(JSON.stringify({
					error: true,
					message: "Your session has expired, please login again"
				}), {
					status: 401,
					headers: {
						"content-type": "application/json"
					}
				});
			}

			return new Response(JSON.stringify({
				error: false,
				user: {
					id: session.id,
					username: session.username + "#" + session.discriminator,
					admin: env.ADMIN_IDS.split(",").includes(session.id)
				}
			}), {
				status: 200,
				headers: {
					"content-type": "application/json"
				}
			});
		} catch (_) {
			return new Response(JSON.stringify({
				error: true,
				message: "Authorization token was malformed"
			}), {
				status: 401,
				headers: {
					"content-type": "application/json"
				}
			});
		}
	} catch (err) {
		let payload = {
			error: true,
			message: "An unknown error occurred"
		};

		if (env.ENVIRONMENT === "development")
			payload.error = err.stack;

		return new Response(JSON.stringify(payload), {
			status: 500,
			headers: {
				"content-type": "application/json"
			}
		});
	}
};
