export const onRequestPost = async ({ request, env }) => {
	try {
		let body = await request.json();
		let { key, id } = body;

		if (!key || !id) {
			return new Response(JSON.stringify({
				error: true,
				message: "Missing body parameters"
			}), {
				status: 400,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		if (key.length !== 128) {
			return new Response(JSON.stringify({
				error: true,
				message: "Invalid key format"
			}), {
				status: 400,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		let verification = await env.VERIFICATIONS.get(id, { type: "json" });

		if (!verification) {
			return new Response(JSON.stringify({
				error: true,
				message: "Pending verification was missing or expired, please request one on the homepage"
			}), {
				status: 422,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		if (key !== verification.key) {
			return new Response(JSON.stringify({
				error: true,
				message: "Verification key was incorrect"
			}), {
				status: 401,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		let promises = [];

		promises.push(env.COMPLETED.get("completed", { type: "json" }));
		promises.push(fetch(`https://discord.com/api/v9/guilds/${env.GUILD_ID}/members/${id}/roles/${env.ROLE_ID}`, {
			method: "PUT",
			headers: {
				authorization: "Bot " + env.BOT_TOKEN,
				"x-audit-log-reason": "CompBot Verification"
			}
		}));

		let [completed, discordResponse] = await Promise.all(promises);

		if (!discordResponse.ok) {
			return new Response(JSON.stringify({
				error: true,
				message: "An error occurred when adding the Member role, please contact a committee member"
			}), {
				status: 500,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		completed.push(verification.email);

		promises = [];

		promises.push(env.VERIFICATIONS.delete(id));
		promises.push(env.COMPLETED.put("completed", JSON.stringify(completed)));

		await Promise.all(promises);

		return new Response(JSON.stringify({
			error: true,
			message: "You should now have Member access to the Discord server"
		}), {
			headers: {
				"content-type": "application/json"
			}
		});
	} catch (err) {
		let payload = {
			error: true,
			message: "An unknown error occurred"
		};

		if (env.ENVIRONMENT === "development")
			payload.error = err.stack;

		return new Response(JSON.stringify(payload), {
			status: 500,
			headers: {
				"content-type": "application/json"
			}
		});
	}
};
