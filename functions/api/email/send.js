/* eslint-disable camelcase */
import * as jose from "jose";
import cookie from "cookie";

const generateID = (length) => {
	let result = "";
	let characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (let i = 0; i < length; i++)
		result += characters.charAt(Math.floor(Math.random() * characters.length));

	return result;
};

export const onRequestPost = async ({ request, env }) => {
	try {
		let cookieHeader = request.headers.get("cookie");

		if (!cookieHeader) {
			return new Response(JSON.stringify({
				error: true,
				message: "You must login with Discord to use this endpoint"
			}), {
				status: 401,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		let cookies = cookie.parse(cookieHeader);
		let token = cookies.token;

		if (!token) {
			return new Response(JSON.stringify({
				error: true,
				message: "You must login with Discord to use this endpoint"
			}), {
				status: 401,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		let publicKey = await jose.importSPKI(env.PUBLIC_KEY, "RS256");
		let session;

		try {
			let { payload } = await jose.jwtVerify(token, publicKey);

			session = await env.SESSIONS.get(payload.id, { type: "json" });

			if (!session) {
				return new Response(JSON.stringify({
					error: true,
					message: "Your session has expired, please login again"
				}), {
					status: 401,
					headers: {
						"content-type": "application/json"
					}
				});
			}
		} catch (_) {
			return new Response(JSON.stringify({
				error: true,
				message: "Authorization token was malformed"
			}), {
				status: 401,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		let body = await request.json();

		if (!body.email) {
			return new Response(JSON.stringify({
				error: true,
				message: "Please enter an email address"
			}), {
				status: 400,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		let verification = await env.VERIFICATIONS.get(session.id, { type: "json" });

		if (verification) {
			return new Response(JSON.stringify({
				error: true,
				message: "You already have a pending verification email, please complete this or wait for it to expire"
			}), {
				status: 422,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		let completed = await env.COMPLETED.get("completed", { type: "json" });

		if (completed.includes(body.email)) {
			return new Response(JSON.stringify({
				error: true,
				message: "This email address has already been used for verification"
			}), {
				status: 401,
				headers: {
					"content-type": "application/json"
				}
			});
		}

		let members = await env.MEMBERS.get("members", { type: "json" });

		let key = generateID(128);

		await env.VERIFICATIONS.put(session.id, JSON.stringify({
			email: body.email,
			key
		}), { expirationTtl: 1800 });

		if (members[body.email]) {
			let url = `${env.BASE_URL}/verify?k=${key}&i=${session.id}`;

			await fetch("https://api.sendgrid.com/v3/mail/send", {
				method: "POST",
				headers: {
					authorization: "Bearer " + env.SENDGRID_API_KEY,
					"content-type": "application/json"
				},
				body: JSON.stringify({
					from: {
						name: "CompSoc Verification",
						email: env.FROM_EMAIL
					},
					template_id: env.EMAIL_TEMPLATE_ID,
					personalizations: [
						{
							to: [
								{
									email: body.email
								}
							],
							dynamic_template_data: {
								url: url
							}
						}
					]
				})
			});
		}

		return new Response(JSON.stringify({
			error: false,
			message: "If you're a CompSoc member, you'll receive a verification email shortly!"
		}), {
			status: 202,
			headers: {
				"content-type": "application/json"
			}
		});
	} catch (err) {
		let payload = {
			error: true,
			message: "An unknown error occurred"
		};

		if (env.ENVIRONMENT === "development")
			payload.error = err.stack;

		return new Response(JSON.stringify(payload), {
			status: 500,
			headers: {
				"content-type": "application/json"
			}
		});
	}
};
