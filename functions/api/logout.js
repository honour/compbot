import cookie from "cookie";

export const onRequestGet = async ({ env }) => {
	let cookieString = cookie.serialize("token", "null", {
		httpOnly: true,
		secure: true,
		path: "/",
		expires: new Date(0)
	});

	return new Response(null, {
		headers: {
			location: env.BASE_URL,
			"set-cookie": cookieString
		},
		status: 302
	});
};
